variable "scaleway_organization" {}
variable "scaleway_token" {}
variable "scaleway_region" {}
variable "provisioner_key_private" {}
variable "provisioner_key_public" {}

provider "scaleway" {
  organization = "${var.scaleway_organization}"
  token        = "${var.scaleway_token}"
  region       = "${var.scaleway_region}"
  version      = "1.8"
}

data "scaleway_image" "docker" {
  architecture = "x86_64"
  name         = "Docker"
}

resource "scaleway_server" "lineage" {
  name      = "lineage"
  image     = "${data.scaleway_image.docker.id}"
  type      = "C2S"
  public_ip = "${scaleway_ip.lineage.ip}"

  volume = {
    type       = "l_ssd"
    size_in_gb = 150
  }

  volume = {
    type       = "l_ssd"
    size_in_gb = 150
  }

  connection {
    type        = "ssh"
    user        = "root"
    agent       = "false"
    private_key = "${var.provisioner_key_private}"
  }

  provisioner "remote-exec" {
    inline = [
      "apt install -y lvm2",
      "systemctl enable lvm2-lvmetad",
      "systemctl start lvm2-lvmetad",
      "pvcreate /dev/nbd1 /dev/nbd2",
      "vgcreate data_vg /dev/nbd1 /dev/nbd2",
      "lvcreate --extents 100%VG -n data_lv data_vg",
      "mkfs.ext4 /dev/data_vg/data_lv",
      "mkdir -p /mnt/data",
      "mount /dev/data_vg/data_lv /mnt/data",
      "fallocate -l4G /swap",
      "chmod 600 /swap",
      "mkswap /swap",
      "swapon /swap",
      "sysctl vm.swappiness=10",
      "sysctl vm.vfs_cache_pressure=50",
    ]
  }

  provisioner "file" {
    source      = "builder/"
    destination = "/mnt/data"
  }

  provisioner "remote-exec" {
    inline = [
      "sh /mnt/data/build.sh",
    ]
  }
}

resource "scaleway_ip" "lineage" {}

resource "scaleway_ssh_key" "terraform" {
  key = "${var.provisioner_key_public}"
}

output "ip" {
  value = "${scaleway_ip.lineage.ip}"
}
