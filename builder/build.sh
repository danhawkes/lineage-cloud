#!/bin/sh

script_dir=$(readlink -f "$(dirname "$0")")

docker run \
    -d \
    -e "CCACHE_SIZE=50G" \
    -e "BRANCH_NAME=lineage-15.1" \
    -e "DEVICE_LIST=h870" \
    -e "WITH_SU=true" \
    -e "SIGN_BUILDS=true" \
    -e "SIGNATURE_SPOOFING=restricted" \
    -e "CUSTOM_PACKAGES=GmsCore GsfProxy FakeStore MozillaNlpBackend NominatimNlpBackend com.google.android.maps.jar FDroid FDroidPrivilegedExtension" \
    -v "${script_dir}/ccache:/srv/ccache" \
    -v "${script_dir}/keys:/srv/keys" \
    -v "${script_dir}/local_manifests:/srv/local_manifests" \
    -v "${script_dir}/logs:/srv/logs" \
    -v "${script_dir}/src:/srv/src" \
    -v "${script_dir}/zips:/srv/zips" \
    --name lineage-builder \
    lineageos4microg/docker-lineage-cicd
