#!/bin/sh

ssh_flags="-o LogLevel=Error -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

ssh_host() {
  echo "root@$(terraform output ip)"
}

start() {
  terraform apply -auto-approve
}

check_status() {
  status=$(ssh $ssh_flags "$(ssh_host)" 'docker ps -a | grep lineage-builder')
  echo "$status" | grep "Up"
}

pull_dir() {
  src="$1"
  dst="$2"
  rsync -zrP -e "ssh ${ssh_flags}" "$(ssh_host):${src}" "${dst}/"
}

pull() {
  dst="output"
  mkdir -p "$dst"
  pull_dir "/mnt/data/logs" "$dst"
  pull_dir "/mnt/data/zips" "$dst"
}

cleanup() {
  terraform destroy -auto-approve
}

start
while check_status
do
  sleep 120
done
pull
cleanup
